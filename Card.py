class Card(object):

    def __init__(self, value, color):
        self.value = value
        self.color = color
        self.game_value = self.set_game_value(value)
        self.display_value = self.set_display_value(value)

    def set_display_value(self, value):
        if(value == 1):
            return 'A'
        elif(value == 11):
            return 'J'
        elif(value == 12):
            return 'Q'
        elif(value == 13):
            return 'K'
        else:
            return str(value)

    def set_game_value(self, value):

        if(value == 1):
            return (1, 11)
        elif(value == 11):
            return 10
        elif(value == 12):
            return 10
        elif(value == 13):
            return 10
        else:
            return value

