import os
from Game import Game

clear_console = lambda: os.system("clear")

my_game =  Game()
print("Ready to play? All the best")

def request_bet():
    while(True):
        try:
            bet = float(raw_input("Please your bet amount $USD: "))
        except:
            print "Please pleace a valid bet:"
        else:
            break
    return bet

def hit_or_stand(is_player = False):
    while(True):
        answer = 's'
        if(is_player == False):
            if(my_game.count_hand(True) > 21):
                break
            elif (my_game.count_hand(True) < my_game.count_hand()):
                break
            elif (my_game.count_hand(True) == my_game.count_hand()):
                answer = 'h'
            elif (my_game.count_hand(False) < 17):
                answer = 'h'
        else:
            answer = raw_input("Hit (any key) or Stand (s): ")

        clear_console()

        if(answer.lower() == "s"):
            break
        else:
            my_game.take_card(is_player)
            my_game.print_hands()

            if(my_game.count_hand(is_player) > 21):
                break


while(len(my_game.Deck.Cards) > 0 and my_game.Player.amount > 0):
    valid_bet = my_game.place_bet(request_bet())

    if(valid_bet):
        my_game.take_card(True)
        my_game.take_card()
        my_game.print_hands()
        hit_or_stand(True)
        hit_or_stand()
        my_game.end_hand()
        


    play_again = raw_input("Do you want to play again? (y/n): ")
    if(play_again.lower() != 'y'):
        break

    if(len(my_game.Deck.Cards) <= 8):
        my_game.Deck.get_new_deck()

print "thanks for playing!! "

    


    