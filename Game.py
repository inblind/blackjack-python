from Deck import Deck
from Player import Player
import random


class Game(object):
    
    def __init__(self):
        self.Player = self.create_player()
        self.Deck = Deck()
        self.Deck.shuffle_deck()
        self.bet = 0
        self.player_hand = []
        self.house_hand = []
        self.house_money = 1000000

    def create_player(self):
        name = str(raw_input("Please enter your name: "))

        while(True):
            try:
                amount = float(raw_input("Please enter the amount of money you will play $USD: "))
            except:
                print ("Please enter a valida amount")
            else:
                break
        
        return Player(name, amount, 1)
    
    def place_bet(self, amount):
        if(amount > self.Player.amount):
            print("This bet is to high, you don't have enough money")
            return False
        elif (amount == 0):
            print("Please pleace a valid bet")
            return False
        else:
            self.bet = amount
            self.Player.amount -= amount
            self.house_hand = []
            self.player_hand = []
            return True
    
    def print_hands(self):

        print "Player {player_name} hand is: ".format(player_name = self.Player.name)
        for card in self.player_hand:
            print "{display_value} {color}".format( display_value = card.display_value, color = card.color)

        print "House hand is: "
        for card in self.house_hand:
            print "{display_value} {color}".format(display_value = card.display_value, color = card.color)
    
    def take_card(self, is_player = False):
        card = self.Deck.pick_card()

        if(is_player):
            self.player_hand.append(card)
        else:
            self.house_hand.append(card)

    def count_hand(self, is_player = False):

        cards = self.player_hand if is_player else self.house_hand
        result = 0
        contains_ace = False
        for card in cards:
            if(card.display_value == 'A'):
                result += int(card.game_value[1])
                contains_ace = True
            else:
                result += card.game_value
        
        if(result > 21 and contains_ace):
            result -= 10

        return result
    
    def end_hand(self):
        player_count = self.count_hand(True) 
        house_count = self.count_hand()

        if(player_count <= 21 and player_count > house_count):
            print "Winner winner chicken dinner"
            self.print_hands()
            self.Player.amount += self.bet * 2
            self.house_money -= self.bet
            self.bet = 0
            self.Player.wins += 1
        elif player_count > 21:
            print "You loose"
            self.house_money += self.bet * 2
            self.bet = 0
            self.Player.losts += 1
        elif house_count > 21:
            print "House loose"
            self.Player.amount += self.bet * 2
            self.house_money -= self.bet
            self.bet = 0
            self.Player.wins += 1
        else:
            print "House Wins"
            self.house_money += self.bet * 2
            self.bet = 0
            self.Player.losts += 1
        
        print "You have an amount of $USD {money} and Wons: {wons} Lost: {lost}".format(money = self.Player.amount, wons = self.Player.wins, lost = self.Player.losts)

