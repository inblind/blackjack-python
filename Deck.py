import random
from Card import Card

class Deck(object):
    
    colors = ['Heart', 'Diamods', 'Spades', 'Clubs']
    def __init__(self):
        self.Cards = [Card(value, color) for value in range(1,14) for color in Deck.colors]
    
    def show_cards(self):
        for card in self.Cards:
            print "Color: {color}, Value: {value}, Display: {display_value}, Game: {game_value}".format(color = card.value, value = card.color, display_value = card.display_value, game_value = card.game_value)

    def shuffle_deck(self):
        random.shuffle((self.Cards))
    
    def pick_card(self):
        return self.Cards.pop()

    def get_new_deck(self):
        self.Cards = [Card(value, color) for value in range(1,14) for color in Deck.colors]
        random.shuffle((self.Cards))
